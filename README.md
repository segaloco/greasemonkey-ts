# Greasemonkey-ts

## Description
Greasemonkey-ts is essentially an npm-powered template for developing a TypeScript-driven Greasemonkey user script.

## Global Node Dependencies
 - concat
 - rimraf
 - typescript
 - webpack
 - webpack-cli

## Building
Simply issue **npm run build-debug** or **npm run build-release** to produce dist.zip which can then be imported into Greasemonkey.

## Remarks
Unfortunately this can only be built on a system with a "zip" command not unlike the one in FreeBSD. I am currently working on https://github.com/gilmorem560/npm-build-zip to address this.
