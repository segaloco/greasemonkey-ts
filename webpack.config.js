const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");

const entry = "./src/index.ts"

const output = {
	filename:	"[name].js",
	path:		path.resolve(__dirname, "dist")
};

const export_module = {
	rules: [{
		test: 		/\.ts$/
		,use:		"ts-loader"
		,exclude:	/node_modules/
	}]
};

const resolve = {
	extensions: [".ts", ".js"]
};

const plugins = [
	new CopyPlugin({
		patterns: [{
			from: "src/.gm.json"
			,to: ".gm.json"
		}]
	})
];

const debug = {
	name:		"debug"
	,mode:		"development"
	,entry:		entry
	,output:	output
	,module:	export_module
	,resolve:	resolve
	,plugins:	plugins
};

const release = {
	name:		"release"
	,mode:		"production"
	,entry:		entry
	,output:	output
	,module:	export_module
	,resolve:	resolve
	,plugins:	plugins
};

module.exports = [ debug, release ];
